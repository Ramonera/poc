Given("I am at {string} website") do |url|
  visit("/vender")
end

Given("I type my email to begin the registration") do
  @page.customer_registration.set_customer_email
  @page.common_actions.start_ad
end

Given("I type the informations about the car") do
  @page.car_registration.car_settings
  @page.common_actions.next_page
  @page.car_information.setting_mileage
  @page.car_information.set_description
  @page.car_information.set_pricing
end

Given("I set the optionals installed in the car") do
  @page.common_actions.next_page
  @page.select_optionals.set_optionals
  @page.common_actions.next_page
  @page.select_optionals.set_features
  @page.common_actions.next_page
end

Given("I set my password") do
  @page.customer_registration.set_customer_password
  @page.common_actions.next_page
  @page.customer_data.set_customer_data
  @page.common_actions.next_page
end

When("I set my plan and the payment data") do
  @page.common_actions.next_page
  @page.payment_plan.set_ad_plan
  @page.payment_plan.credit_card
  @page.payment_plan.set_license_plate
  @page.common_actions.agree_terms
  @page.common_actions.next_page
end

Then("I hope the success screen has been show") do
  find(MAP["confirMsg"]).assert_text("Seu pagamento foi processado!")
end

#pagamento com boleto
When("I set my plan and the payment data as a boleto") do
  @page.common_actions.next_page
  @page.payment_plan.set_ad_plan
  @page.payment_plan.set_payment_boleto
  @page.payment_plan.set_license_plate
  @page.common_actions.agree_terms
  @page.common_actions.next_page
end

Then("I hope the success screen of boleto has been show") do
  assert_text("Seu boleto foi gerado!")
end
