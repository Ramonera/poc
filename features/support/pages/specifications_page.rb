module HVender
  # this class set the main information of the vehicle
  class CarSettings
    include Capybara::DSL

    def car_settings
      assert_no_selector(MAP['loader'], wait: 40)
      # seleciona o campo que contem as marcas e seleciona uma das listadas
      sleep(2)
      all(MAP['dropMake'], wait: 15)[rand(2..13)].click
      # seleciona o campo que contem os modelos e seleciona um dos listados
      all(MAP['dropModel'], wait: 15)[rand(2..all(MAP['dropModel'], wait: 10).count - 1)].click
      # seleciona o ano do modelo
      all(MAP['dropYrMod'], wait: 15)[rand(1..all(MAP['dropYrMod'], wait: 10).count - 1)].click
      # seleciona o ano de fabricação
      all(MAP['dropYrMan'], wait: 15)[rand(1..all(MAP['dropYrMan'], wait: 10).count - 1)].click
      # seleciona a versão do veiculo
      sleep(2)
      all(MAP['dropVersion'], wait: 15)[rand(1..all(MAP['dropVersion'], wait: 20).count - 1)].click
      # seleciona a cor do veiculo
      all(MAP['dropColor'], wait: 15)[rand(1..all(MAP['dropColor'], wait: 20).count - 1)].click
      # select the type of transmition if the selector visibility is true
      all(MAP['dropTransmition'], wait: 15)[rand(1..all(MAP['dropTransmition'], wait: 20).count - 1)].click if has_xpath?(MAP['dropTransmition'],wait: 5)
    end
  end
end
