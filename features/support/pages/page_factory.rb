module HVender
  # this class invoke all the other classes of page object
  class PageFactory
    def car_registration
      HVender::CarSettings.new
    end

    def car_information
      HVender::InformationVehicle.new
    end

    def common_actions
      HVender::CommonActions.new
    end

    def select_optionals
      HVender::SettingOptionals.new
    end

    def select_features
      HVender::SettingOptionals.new
    end

    def customer_registration
      HVender::CredentialsVender.new
    end

    def customer_data
      HVender::CustomerData.new
    end

    def payment_plan
      HVender::PaymentPlan.new
    end
  end
end
