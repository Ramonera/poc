# name spacing from Vender Webmotors
module HVender
  # class to fill in the customer data in the hvender course
  class CustomerData
    include Capybara::DSL

    def set_customer_data
      user = FactoryBot.attributes_for(:user_1)
      find(:xpath, MAP['inputName'], wait: 10).set(user[:name_customer])
      find(:xpath, MAP['inputBirthDt'], wait: 10).set(user[:dt_birth])
      find(:xpath, MAP['inputCPF'], wait: 10).set(user[:cpf])
      find(:xpath, MAP['inpuCEP'], wait: 10).set(user[:cep])
      find(:xpath, MAP['inputPhone'], wait: 10).set(user[:tel])
    end
  end
end
