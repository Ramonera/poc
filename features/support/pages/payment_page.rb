module HVender
  # page to setting the method of payment with a credit card or boleto and the license plate of a vehicle
  class PaymentPlan
    include Capybara::DSL

    def set_ad_plan
      assert_text('Agora escolha o melhor plano para você', wait: 20)
      find(:xpath, MAP['buttonPlanPayment'], wait: 20).click
    end

    def credit_card
      find(:xpath, MAP['inputCreditCard'], wait: 5).set(MAP['ncartao'])
      find(:xpath, MAP['inputCreditCrdName']).set(FFaker::NameBR.name)
      find(:xpath, MAP['inputCreditCVV']).set('312')
      find(:xpath, MAP['inputValidThru']).set('10/25')
    end

    def set_payment_boleto
      user = FactoryBot.attributes_for(:user_1)
      find(:xpath, MAP['btnBoleto'], text: 'BOLETO').click
      find(:xpath, MAP['inputEndereco']).set(user[:rua])
      find(:xpath, MAP['inputNumero']).set(user[:numero])
      find(:xpath, MAP['inputComplemento']).set(user[:complemento])
    end

    def set_license_plate
      find(:xpath, MAP['inputLicensePlate']).set(('a'..'z').to_a.sample(3).join.upcase + rand(1000..9999).to_s)
    end
  end
end
