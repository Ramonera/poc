module HVender
  # set the mileage, description and princing of a vehicle
  class InformationVehicle
    include Capybara::DSL

    def setting_mileage
      find(:xpath, MAP["divVehicleMile"], wait: 20).all(:xpath, MAP["inputVehicleMile"], wait: 10).first.set(rand(0..100000).to_s)
    end

    def set_description
      find(:xpath, MAP["inputDescription"], wait: 2).set(FFaker::Lorem.paragraph)
    end

    def set_pricing
      if has_selector?(:xpath, MAP["spanFipe"], wait: 10)
        preco = find(:xpath, MAP["spanFipe"]).text
        preco.delete!("R$")
        preco.gsub(/,(?=\d{2}\b)/, "")
        find(:xpath, MAP["inputPrice"]).set(preco)
      else
        find(:xpath, MAP["inputPrice"]).set(rand(20000..100_000), wait: 20)
      end
    end
  end
end
