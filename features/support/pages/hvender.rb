class Hvender
  include Capybara::DSL


  # def setPaymentPlan
  #   assert_text("Agora escolha o melhor plano para você", wait: 20)
  #   sleep(2)
  #   find(:xpath, MAP["buttonPlanPayment"], wait: 20).click
  #   find(:xpath, MAP["inputCreditCard"], wait: 5).set(MAP["ncartao"])
  #   find(:xpath, MAP["inputCreditCrdName"]).set(FFaker::NameBR.name)
  #   find(:xpath, MAP["inputCreditCVV"]).set("312")
  #   find(:xpath, MAP["inputValidThru"]).set("10/25")
  #   find(:xpath, MAP["inputLicensePlate"]).set(("a".."z").to_a.sample(3).join.upcase + rand(1000..9999).to_s) # -> método pra gerar placas
  #   find(:xpath, MAP["checkTerms"]).click
  #   find(:xpath, MAP["btnNextRed"]).click
  #   assert_no_selector(:xpath, MAP["loader"], wait: 40)
  # end

  def setPaymentPlanBoleto
    find(:xpath, MAP["btnBoleto"], text: "BOLETO").click
    find(:xpath, MAP["inputEndereco"]).set(MAP["endereco"])
    find(:xpath, MAP["inputNumero"]).set(MAP["numero"])
    find(:xpath, MAP["inputComplemento"]).set(MAP["complemento"])
    find(:xpath, MAP["inputLicensePlate"]).set(("a".."z").to_a.sample(3).join.upcase + rand(1000..9999).to_s) # -> método pra gerar placas
    find(:xpath, MAP["checkTerms"]).click
    find(:xpath, MAP["btnNextRed"]).click
    assert_no_selector(:xpath, MAP["loader"], wait: 40)
  end
end
