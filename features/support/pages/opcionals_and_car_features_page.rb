module HVender
  # set the number of optionals of a vehicle
  class SettingOptionals
    include Capybara::DSL

    def set_optionals
      nr_optionals = rand(1..5)
      while nr_optionals >= all(MAP['chkSpan']).count
        find("//div[@class= 'tags']//span[#{rand(1..all(MAP['spanFeatures']).count)}]").click
      end
    end

    def set_features
      nr_optionals = rand(1..5)
      while nr_optionals >= all(MAP['chkSpan']).count
        find("//div[@class= 'tags']//span[#{rand(1..all(MAP['spanFeatures']).count)}]").click
      end
    end
  end
end
