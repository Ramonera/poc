module HVender
  # this class set the credentials of a customer who wants to create a advertisement
  class CredentialsVender
    include Capybara::DSL

    def set_customer_email
      user = FactoryBot.attributes_for(:user_1)
      find(:xpath, MAP["inputEmail"], wait: 20).set(user[:email])
    end

    def set_customer_password
      user = FactoryBot.attributes_for(:user_1)
      find(:xpath, MAP["inputPass"], wait: 20).set(user[:pass])
    end
  end
end
