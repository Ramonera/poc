module HVender
  # this class contains all common methods to all script
  class CommonActions
    include Capybara::DSL

    def next_page
      find(MAP["btnNextRed"], wait: 20).click
      assert_no_selector(MAP["loader"], wait: 40)
    end

    def start_ad
      find(MAP["startAd"]).click
    end

    def agree_terms
      find(MAP["checkTerms"]).click
    end
  end
end
