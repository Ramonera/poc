require 'selenium-webdriver'
require 'report_builder'
require 'factory_bot'

Before do
  @Hvender = Hvender.new
  @page = HVender::PageFactory.new
  page.driver.browser.manage.window.resize_to(1920, 1080)
  page.driver.browser.manage.window.maximize
end

# After do |scenario|
#   if scenario.failed? do
#     abort
#   end
# end

ReportBuilder.configure do |config|
  config.json_path = 'data/reports/report.json'
  config.report_path = 'data/reports/poc_webmotors'
  config.report_types = [:html]
  config.report_title = 'Report POC WebMotors'
  config.include_images = true
  config.additional_info = { browser: 'Chrome', environment: $env }
end

# After do |scenario|
#   binding.pry if scenario.failed?
# end

After do |scenario|
  file_name = scenario.name.tr(' ', '_').downcase!
  target = "data/screenshots/error/#{file_name}.png"
  if scenario.failed?
    page.save_screenshot(target)
    embed(target, 'image/png', 'Evidência')
  end
  Capybara.current_session.driver.quit
end

at_exit do
  ReportBuilder.build_report
end
