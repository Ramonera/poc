# Library to create a vehicle information
class Customer
  FactoryBot.define do
    factory :user_1, :class => :customer do |u|
      email { FFaker::Internet.email }
      pass {'teste123'}
      name_customer {FFaker::NameBR.name}
      dt_birth {'01011990'}
      cpf { FFaker::IdentificationBR.cpf }
      tel { FFaker::PhoneNumberBR.home_work_phone_number }
      cep { '04547006' }
      rua {'Rua dos Bobos'}
      numero {'2000'}
      complemento {'Apartamento 201'}
    end
  end
end
