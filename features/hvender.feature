Feature: Register a car on the catalog

  Context: I as a user, I would like to make an announcement on the site hvender in order to sell my car

@cartao
  Scenario: Register a car on hvender
    Given I am at "hvender" website
    And I type my email to begin the registration
    And I type the informations about the car
    And I set the optionals installed in the car
    And I set my password
    When I set my plan and the payment data
    Then I hope the success screen has been show

@boleto
  Scenario: Register a car on hvender with payment method as boleto
    Given I am at "hvender" website
    And I type my email to begin the registration
    And I type the informations about the car
    And I set the optionals installed in the car
    And I set my password
    When I set my plan and the payment data as a boleto
    Then I hope the success screen of boleto has been show